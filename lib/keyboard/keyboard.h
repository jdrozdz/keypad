/*
 * keyboard.h
 *
 *  Created on: 17 sty 2015
 *      Author: jerzyk
 */

#ifndef LIB_KEYBOARD_KEYBOARD_H_
#define LIB_KEYBOARD_KEYBOARD_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include "../lcd/HD44780.h"

// Columns
#define K1_DIR		DDRA
#define K1_PORT		PORTA
#define K1_PIN		PINA
#define K1			(1 << PA0)

#define K2_DIR		DDRA
#define K2_PORT		PORTA
#define K2_PIN		PINA
#define K2			(1 << PA1)

#define K3_DIR		DDRA
#define K3_PORT		PORTA
#define K3_PIN		PINA
#define K3			(1 << PA2)

#define K4_DIR		DDRA
#define K4_PORT		PORTA
#define K4_PIN		PINA
#define K4			(1 << PA3)

// Rows
#define W1_DIR		DDRA
#define W1_PORT		PORTA
#define W1			(1 << PA4)

#define W2_DIR		DDRA
#define W2_PORT		PORTA
#define W2			(1 << PA5)

#define W3_DIR		DDRA
#define W3_PORT		PORTA
#define W3			(1 << PA6)

#define W4_DIR		DDRA
#define W4_PORT		PORTA
#define W4			(1 << PA7)

#define COL1		(K1_PIN & K1)
#define COL2		(K2_PIN & K2)
#define COL3		(K3_PIN & K3)
#define COL4		(K4_PIN & K4)

#define ROW1_SET	W1_PORT |= W1
#define ROW1_CLR	W1_PORT &= ~W1
#define ROW2_SET	W2_PORT |= W2
#define ROW2_CLR	W2_PORT &= ~W2
#define ROW3_SET	W3_PORT |= W3
#define ROW3_CLR	W3_PORT &= ~W3
#define ROW4_SET	W4_PORT |= W4
#define ROW4_CLR	W4_PORT &= ~W4

typedef struct {
	char *key_value;
} KEYBOARD;

// Keyboard structure
extern KEYBOARD keyboard;

// Initialize keypad
void KeypadInitialize(void);

// General function witch key was pressed
void ReadKeypad(void);

#endif /* LIB_KEYBOARD_KEYBOARD_H_ */
