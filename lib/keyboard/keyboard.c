/*
 * keyboard.c
 *
 *  Created on: 17 sty 2015
 *      Author: jerzyk
 */

#include "keyboard.h"


KEYBOARD keyboard;

void KeypadInitialize(void){
	W1_DIR |= W1; W2_DIR |= W2;
	W3_DIR |= W1; W4_DIR |= W4;
}

void ReadKeypad(void){
	keyboard.key_value = " ";
	/// First row
	ROW1_CLR;
	if(!COL1){
		keyboard.key_value = "1";
	}
	if(!COL2){
		keyboard.key_value = "2";
	}
	if(!COL3){
		keyboard.key_value = "3";
	}
	if(!COL4){
		keyboard.key_value = "4";
	}
	ROW1_SET;

	/// Second row
	ROW2_CLR;
	if(!COL1){
		keyboard.key_value = "5";
	}
	if(!COL2){
		keyboard.key_value = "6";
	}
	if(!COL3){
		keyboard.key_value = "7";
	}
	if(!COL4){
		keyboard.key_value = "8";
	}
	ROW2_SET;

	/// Third row
	ROW3_CLR;
	if(!COL1){
		keyboard.key_value = "9";
	}
	if(!COL2){
		keyboard.key_value = "0";
	}
	if(!COL3){
		keyboard.key_value = "A";
	}
	if(!COL4){
		keyboard.key_value = "B";
	}
	ROW3_SET;

	/// Fourth row
	ROW4_CLR;
	if(!COL1){
		keyboard.key_value = "C";
	}
	if(!COL2){
		keyboard.key_value = "D";
	}
	if(!COL3){
		keyboard.key_value = "E";
	}
	if(!COL4){
		keyboard.key_value = "F";
	}
	ROW4_SET;
}
