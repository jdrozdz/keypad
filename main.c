/*
 * main.c
 *
 *  Created on: 8 sty 2015
 *      Author: jerzyk
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <string.h>

#include "lib/lcd/HD44780.h"
#include "lib/keyboard/keyboard.h"

int main(void){

	// LED
	DDRD |= (1 << PD6) | (1 << PD5);
	PORTD |= (0 << PD6) | (0 << PD5);

	KeypadInitialize();

	LCD_Initalize();
	LCD_WriteText("Keypad test");
	_delay_ms(1700);
	LCD_Clear();

	while(1){
		ReadKeypad();
		LCD_GoTo(0,0);
		LCD_WriteText("Pressed:");
		LCD_WriteText(keyboard.key_value);
		_delay_ms(700);
	}

	return 0;
}
